package test;

import matrixLib.*;
import matrixLib.matrixFunctions.calculators.MatrixDeterminant;
import matrixLib.support.datatypes.HalfByte;
import matrixLib.support.datatypes.UnsignedInteger;
import matrixLib.support.operations.*;

import java.io.IOException;


public class Main {

    public static void main(String[] args) throws IOException {
        Integer[][] matrix= {{0, 1}, {2,3}};
        char[][] badMatrix = {{'a','b'},{'a','b'}};
        Number[][] crazyMatrix = {{5, 2.5}, {new UnsignedInteger(4), -2}};

/*
        Matrix<Integer> matrix2 = new Matrix<>(matrix);
        matrix2.printMatrix();
        matrix2.generateRandom(5,4);
        matrix2.printMatrix();



        Byte[][] matrixbyte = {{-6, 5, 13}, {4, 3, 9}};
        Matrix<Byte> matrixByte = new Matrix<>(matrixbyte);
        matrixByte.printMatrix();
        matrixByte.generateRandom(3, 2);
        matrixByte.printMatrix();


        MatrixPrinter printer = new MatrixPrinter();
        MatrixGenerator<UnsignedInteger> unsignedIntegerMatrixGenerator = new MatrixGenerator<>(2, 4, new UnsignedIntegerOperations());
        Matrix<UnsignedInteger> uiMatrix = unsignedIntegerMatrixGenerator.randomMatrix();
        printer.printMatrix(uiMatrix.getMatrix());
        uiMatrix.generateRandom(2, 4);
        printer.printMatrix(uiMatrix);



        MatrixGenerator<HalfByte> gen = new MatrixGenerator<>(3,3, new HalfByteOperations());

        Matrix<HalfByte> matica1 = gen.randomMatrix();
        Matrix<HalfByte> matica2 = gen.randomMatrix();
        matica1.printMatrix();
        matica2.printMatrix();


        MatrixMultiplier<Integer> calc = new MatrixMultiplier<Integer>(new IntegerOperations());
        MatrixGenerator<Integer> gen2 = new MatrixGenerator<>(3,3, new IntegerOperations());
        Integer[][] matica3 = {{1, 2}, {4,6}};
        Integer[][] matica4 = {{3,4}, {0, 1}};
        Matrix<Integer> matica31 = new Matrix<>(matica3);
        Matrix<Integer> matica41 = new Matrix<>(matica4);
        matica31.printMatrix();
        matica41.printMatrix();

        Matrix<Integer> matica666 = calc.multiplication(matica31, matica41);
        matica666.printMatrix();

        Matrix<Integer> powMatica41 = matica41.multiply(matica41);
        powMatica41.printMatrix();

        MatrixCalculator<Integer> calculator = new MatrixCalculator<>();
        Matrix<Integer> matica31plus41 = calculator.addition(matica31, matica41);
        matica31plus41.printMatrix();


        MatrixExporter exporter = new MatrixExporter();
        exporter.exportToCSV("matica31plus41", matica31plus41);

        MatrixImporter<Integer> importer = new MatrixImporter<>(new IntegerOperations());
        Matrix<Integer> importedMatica = importer.importFromCSV("matica31plus41");

        importedMatica.printMatrix();
*/


        Matrix<Double> matrixDouble1 = new Matrix<Double>(new DoubleOperations());
        Matrix<Double> matrixDouble2 = new Matrix<Double>(new DoubleOperations());

        matrixDouble2.generateRandom(5,5);
        matrixDouble2.printMatrix();

        matrixDouble2.exportToCSV("matrixDouble");

        matrixDouble1.importFromCSV("matrixDouble");
        matrixDouble1.printMatrix();

        matrixDouble1.multiply(matrixDouble2).printMatrix();


        Integer[][] matInt1 ={{4,5,-3, 1},{4,-2, 0, 1},{3, 3, 3, 1},{1,1,1,1}};
        Integer[][] matInt2 ={{1,7,11, 1},{0,1, -5, 1}, {6, -6, 6, 1},{1,1,1,1}};
        Matrix<Integer> matrixInt1 = new Matrix<Integer>(matInt1);
        Matrix<Integer> matrixInt2 = new Matrix<Integer>(matInt2);

        matrixInt1.minus(matrixInt2).printMatrix();
        matrixInt1.multiply(matrixInt2).printMatrix();

        Matrix<UnsignedInteger> matrixUI1 = new Matrix<UnsignedInteger>(new UnsignedIntegerOperations());
        Matrix<UnsignedInteger> matrixUI2 = new Matrix<UnsignedInteger>(new UnsignedIntegerOperations());

        matrixUI1.generateRandom(3,3);
        matrixUI2.generateRandom(3,3);

        matrixUI1.printMatrix();
        matrixUI2.printMatrix();


        Integer detResult = matrixInt1.getDeterminant();
        System.out.println(detResult);
        

    }

}
