package matrixLib.matrixFunctions.calculators;

import matrixLib.Matrix;
import matrixLib.support.operations.GenericOperations;

public class MatrixAdditioner<MatrixType extends Number> extends MatrixCalculatorMemory<MatrixType> {
    public MatrixAdditioner(GenericOperations<MatrixType> operations){
        super(operations);
    }

    @Override
    protected void setMatrices(MatrixType[][] matrix1, MatrixType[][] matrix2){
        this.matrixChecker.throwExceptionIfAdditionIsNotPossible(matrix1, matrix2);

        super.setMatrices(matrix1, matrix2);
    }

    @Override
    protected MatrixType calculateOneItem(int idColumn, int idRow) {
        MatrixType number1 = this.matrix1[idRow][idColumn];
        MatrixType number2 = this.matrix2[idRow][idColumn];

        return this.operations.plus(number1, number2);
    }


    public MatrixType[][] addition(MatrixType[][] matrix1, MatrixType[][] matrix2){
        MatrixType[][] matrix = setUp(matrix1, matrix2);

        return calculateResultMatrix(matrix);
    }

    public Matrix<MatrixType> addition(Matrix<MatrixType> matrix1, Matrix<MatrixType> matrix2){
        this.matrixChecker.throwExceptionIfNull(matrix1);
        this.matrixChecker.throwExceptionIfNull(matrix2);

        MatrixType[][] resultArrayMatrix = addition(matrix1.getMatrix(), matrix2.getMatrix());

        return new Matrix<>(resultArrayMatrix);
    }

}
