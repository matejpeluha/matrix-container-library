package matrixLib.matrixFunctions.calculators;

import matrixLib.Matrix;
import matrixLib.support.operations.GenericOperations;

import java.awt.*;

public class MatrixMultiplier<MatrixType extends Number> extends MatrixCalculatorMemory<MatrixType>{

    public MatrixMultiplier(GenericOperations<MatrixType> operations){
        super(operations);
    }

    @Override
    protected Dimension getResultMatrixSize(){
        this.matrixChecker.throwExceptionIfNull(this.matrix2[0]);

        int resultMatrixNumberOfRows = this.matrix1.length;
        int resultMatrixNumberOfColumns = this.matrix2[0].length;

        return new Dimension(resultMatrixNumberOfColumns, resultMatrixNumberOfRows);
    }

    @Override
    protected void setMatrices(MatrixType[][] matrix1, MatrixType[][] matrix2){
        this.matrixChecker.throwExceptionIfMatricesCanNotBeMultiplied(matrix1, matrix2);

        super.setMatrices(matrix1, matrix2);
    }

    @Override
    protected MatrixType calculateOneItem(int idColumn, int idRow){
        final int MAX_SIZE = this.matrix2.length;

        MatrixType[] matrix1Column = this.matrix1[idRow];

        MatrixType idCalculation;
        MatrixType item = this.operations.initializeZero();


        for(int id = 0; id < MAX_SIZE; id++){
            idCalculation = operations.multiply(matrix1Column[id], this.matrix2[id][idColumn]);
            item = this.operations.plus(item, idCalculation);
        }

        return item;
    }

    public MatrixType[][] multiplication(MatrixType[][] matrix1, MatrixType[][] matrix2){
        MatrixType[][] matrix = setUp(matrix1, matrix2);

        return calculateResultMatrix(matrix);
    }

    public Matrix<MatrixType> multiplication(Matrix<MatrixType> matrix1, Matrix<MatrixType> matrix2){
        this.matrixChecker.throwExceptionIfNull(matrix1);
        this.matrixChecker.throwExceptionIfNull(matrix2);

        MatrixType[][] resultArrayMatrix = multiplication(matrix1.getMatrix(), matrix2.getMatrix());

        return new Matrix<>(resultArrayMatrix);
    }
}
