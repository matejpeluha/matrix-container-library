package matrixLib.matrixFunctions.calculators;

import matrixLib.Matrix;
import matrixLib.support.MatrixChecker;
import matrixLib.support.datatypes.UnsignedInteger;
import matrixLib.support.operations.GenericOperations;

import java.awt.*;

public class MatrixDeterminant<MatrixType extends Number> {
    private final MatrixChecker matrixChecker = new MatrixChecker();
    private final GenericOperations<MatrixType> operations;


    public MatrixDeterminant(GenericOperations<MatrixType> operations){
        matrixChecker.throwExceptionIfNull(operations);

        this.operations = operations;
    }

    private MatrixType getDeterminantOfDimension2(MatrixType[][] matrix){
        matrixChecker.throwExceptionIfNull(matrix[0]);
        matrixChecker.throwExceptionIfNull(matrix[0][0]);

        MatrixType number1 = operations.multiply(matrix[0][0], matrix[1][1]);
        MatrixType number2 = operations.multiply(matrix[0][1], matrix[1][0]);

        return operations.minus(number1, number2);
    }


    private MatrixType[][] saveMatrixPartInTemporary(MatrixType[][] matrix, MatrixType[][] temporary, Point matrixPoint){
        matrixChecker.throwExceptionIfNull(matrix);
        matrixChecker.throwExceptionIfNull(temporary);

        int idColumn = matrixPoint.x;
        int idRow = matrixPoint.y;

        for (int id = 0; id < matrix[0].length; id++) {
            if (id < idColumn) {
                temporary[idRow - 1][id] = matrix[idRow][id];
            } else if (id > idColumn) {
                temporary[idRow - 1][id - 1] = matrix[idRow][id];
            }
        }

        return temporary;
    }


    private MatrixType[][] saveMatrixPartInTemporary(MatrixType[][] matrix, int idColumn){
        int temporaryNumRows = matrix.length - 1;
        int temporaryNumColumns = matrix[0].length - 1;

        MatrixType[][] temporary = operations.createArrayMatrix(temporaryNumRows, temporaryNumColumns);

        for (int idRow = 1; idRow < matrix.length; idRow++) {
            Point matrixPoint = new Point(idColumn, idRow);
            temporary = saveMatrixPartInTemporary(matrix, temporary, matrixPoint);
        }

        return temporary;
    }


    private MatrixType calculateResultUpdate(MatrixType leader, MatrixType sign, MatrixType[][] temporary){
        MatrixType number1 = operations.multiply(leader, sign);

        return operations.multiply(number1, getDeterminant(temporary));
    }

    private MatrixType getDeterminantOfBigDimensions(MatrixType[][] matrix){
        MatrixType result = operations.initializeZero();

        final MatrixType ZERO = operations.initializeZero();
        final MatrixType signConverter = operations.decrement(ZERO);

        MatrixType sign = operations.increment(ZERO);

        for (int idColumn = 0; idColumn < matrix[0].length; idColumn++) {
            MatrixType[][] temporary = saveMatrixPartInTemporary(matrix, idColumn);

            MatrixType resultUpdate = calculateResultUpdate(matrix[0][idColumn], sign, temporary);

            result = operations.plus(result, resultUpdate);

            sign = operations.multiply(sign, signConverter);
        }

        return result;
    }

    public MatrixType getDeterminant (MatrixType[][] matrix) {
        matrixChecker.throwExceptionIfDeterminantIsNotPossible(matrix);
        matrixChecker.findPossibleExceptionsInMatrix(matrix);

        if(matrix instanceof UnsignedInteger[][])
            throw new IllegalArgumentException("Pre matica typu UnsignedInteger nemoze vypocitat determinant.");

        if (matrix.length == 1)
            return matrix[0][0];

        else if (matrix.length == 2) {
            return getDeterminantOfDimension2(matrix);
        }

        return getDeterminantOfBigDimensions(matrix);
    }

    public MatrixType getDeterminant(Matrix<MatrixType> matrix){
        matrixChecker.throwExceptionIfNull(matrix);

        MatrixType[][] arrayMatrix = matrix.getMatrix();

        return getDeterminant(arrayMatrix);
    }
}
