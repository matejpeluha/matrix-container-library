package matrixLib.matrixFunctions.calculators;


import matrixLib.support.MatrixChecker;
import matrixLib.support.operations.GenericOperations;
import java.awt.*;


public abstract class MatrixCalculatorMemory<MatrixType extends Number> {
    protected MatrixType[][] matrix1;
    protected MatrixType[][] matrix2;

    protected final GenericOperations<MatrixType> operations;
    protected final MatrixChecker matrixChecker = new MatrixChecker();

    public MatrixCalculatorMemory(GenericOperations<MatrixType> operations){
        this.operations = operations;
    }

    protected Dimension getResultMatrixSize(){
        int resultMatrixNumberOfRows = this.matrix1.length;
        int resultMatrixNumberOfColumns = this.matrix1[0].length;

        return new Dimension(resultMatrixNumberOfColumns, resultMatrixNumberOfRows);
    }

    protected void setMatrices(MatrixType[][] matrix1, MatrixType[][] matrix2){
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
    }

    protected MatrixType[][] setUp(MatrixType[][] matrix1, MatrixType[][] matrix2){
        setMatrices(matrix1, matrix2);

        Dimension resultMatrixSize = getResultMatrixSize();

        return createEmptyResultMatrix(resultMatrixSize);
    }

    protected MatrixType[][] createEmptyResultMatrix(Dimension size){
        this.matrixChecker.throwExceptionIfNull(size);

        return this.operations.createArrayMatrix(size.height, size.width);
    }

    protected abstract MatrixType calculateOneItem(int idColumn, int idRow);

    protected MatrixType[] calculateOneRow(MatrixType[] row, int idRow){
        this.matrixChecker.throwExceptionIfNull(row);
        int numberOfColumns = row.length;

        for(int idColumn = 0; idColumn < numberOfColumns; idColumn++)
            row[idColumn] = calculateOneItem(idColumn, idRow);

        return row;
    }

    protected MatrixType[][] calculateResultMatrix(MatrixType[][] matrix){
        this.matrixChecker.throwExceptionIfNull(matrix);
        int numberOfRows = matrix.length;

        for(int idRow = 0; idRow < numberOfRows; idRow++)
            matrix[idRow] = calculateOneRow(matrix[idRow], idRow);

        return matrix;
    }

}
