package matrixLib.matrixFunctions;

import javafx.scene.transform.MatrixType;
import lombok.Setter;
import matrixLib.Matrix;
import matrixLib.support.MatrixChecker;
import matrixLib.support.operations.GenericOperations;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class MatrixImporter<MatrixType extends Number> {
    @Setter
    private String path;
    private final GenericOperations<MatrixType> operations;
    private final MatrixChecker matrixChecker = new MatrixChecker();

    public MatrixImporter(GenericOperations<MatrixType> operations) {
        matrixChecker.throwExceptionIfNull(operations);

        this.path = System.getProperty("user.dir");
        this.path += "\\src\\csvFiles\\";

        this.operations = operations;
    }

    public MatrixImporter(String path, GenericOperations<MatrixType> operations) {
        matrixChecker.throwExceptionIfNull(operations);
        matrixChecker.throwExceptionIfNull(path);

        this.path = path;

        this.operations = operations;

    }


    private ArrayList<String[]> getStringFromCSV(String path) throws IOException {
        FileReader reader = new FileReader(path);
        BufferedReader csvReader = new BufferedReader(reader);
        String row;
        ArrayList<String[]> stringFromCSV = new ArrayList<String[]>();

        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            stringFromCSV.add(data);
        }


        csvReader.close();
        reader.close();

        return stringFromCSV;
    }

    private MatrixType[] transferNumbersInRow(MatrixType[] destination, String[] source){
        final int destinationNumberOfColumns = destination.length;

        for(int idColumn = 0; idColumn < destinationNumberOfColumns; idColumn++) {
            String stringNumber = source[idColumn];
            MatrixType number = operations.castFromString(stringNumber);
            destination[idColumn] = number;
        }

        return destination;
    }

    private MatrixType[][] transferNumbers(MatrixType[][] destination, ArrayList<String[]> source){
        matrixChecker.throwExceptionIfNull(destination);

        final int destinationNumberOfRows = destination.length;

        for(int idRow = 0; idRow < destinationNumberOfRows; idRow++)
            destination[idRow] = transferNumbersInRow(destination[idRow], source.get(idRow));

        return destination;
    }

    private MatrixType[][] convertToArrayMatrix(ArrayList<String[]> stringMatrix){
        matrixChecker.throwExceptionIfNull(stringMatrix);

        int numRows = stringMatrix.size();
        int numColumns = stringMatrix.get(0).length;

        MatrixType[][] arrayMatrix = operations.createArrayMatrix(numRows, numColumns);

        return transferNumbers(arrayMatrix, stringMatrix);
    }

    private Matrix<MatrixType> readFromCSV(String csvFilePath) throws IOException {
        matrixChecker.throwExceptionIfNull(csvFilePath);

        ArrayList<String[]> stringMatrix = getStringFromCSV(csvFilePath);

        MatrixType[][] arrayMatrix = convertToArrayMatrix(stringMatrix);

        System.out.println("Uspesne importovana matica z " + csvFilePath);

        return new Matrix<>(arrayMatrix);
    }

    public Matrix<MatrixType> importFromCSV(String nameFile) {
        matrixChecker.throwExceptionIfNull(nameFile);

        final String csvFilePath = this.path + nameFile + ".csv";

        try{
            return readFromCSV(csvFilePath);
        }catch (IOException e){
            System.out.println("Neuspesne importovana matica z " + csvFilePath);
            return new Matrix<>(this.operations);
        }
    }

}
