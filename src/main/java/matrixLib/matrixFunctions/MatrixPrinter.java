package matrixLib.matrixFunctions;

import matrixLib.Matrix;
import matrixLib.support.datatypes.HalfByte;
import matrixLib.support.MatrixChecker;
import matrixLib.support.datatypes.UnsignedInteger;

public class MatrixPrinter {
    private final MatrixChecker matrixChecker = new MatrixChecker();

    private void printNumber(Number number){
        this.matrixChecker.throwExceptionIfNull(number);

        if(number instanceof UnsignedInteger || number instanceof HalfByte)
            System.out.print(number.intValue() + " ");
        else
            System.out.print(number + " ");
    }

    private void printRow(Number[] row){
        this.matrixChecker.throwExceptionIfNull(row);

        System.out.print("[ ");
        for(Number number : row)
            printNumber(number);
        System.out.print("]");
    }

    public void printMatrix(Number[][] matrix){
        if(matrix == null)
            return;

        matrixChecker.findPossibleExceptionsInMatrix(matrix);

        for(Number[] row : matrix) {
            printRow(row);
            System.out.println("");
        }

        System.out.println("");
    }

    public void printMatrix(Matrix<? extends Number> matrix){
        if(matrix != null)
            printMatrix(matrix.getMatrix());
    }

}
