package matrixLib.matrixFunctions;

import lombok.Setter;
import matrixLib.Matrix;
import matrixLib.support.MatrixChecker;

import java.io.FileWriter;
import java.io.IOException;

public class MatrixExporter {
    @Setter
    private String path;

    private FileWriter writer;
    private final MatrixChecker matrixChecker = new MatrixChecker();

    public MatrixExporter() {
        this.path = System.getProperty("user.dir");
        this.path += "\\src\\csvFiles\\";
    }

    public MatrixExporter(String path) {
        matrixChecker.throwExceptionIfNull(path);

        this.path = path;
        this.path += "\\";
    }

    private String[] getStringRow(Number[] row){
        String[] stringRow = new String[row.length];
        int i = 0;

        for(Number number : row)
            stringRow[i++] = number.toString();

        return stringRow;
    }


    private void writeRow(Number[] row) throws IOException {
        matrixChecker.throwExceptionIfNull(row);

        String[] stringRow = getStringRow(row);

        this.writer.append(String.join(", ", stringRow));

        this.writer.append("\n");
    }

    private void writeMatrix(Number[][] matrix) throws IOException {
        matrixChecker.throwExceptionIfNull(matrix);

        for(Number[] row : matrix)
            writeRow(row);
    }

    private void writeToCSV(String path, Matrix<? extends Number> matrix) throws IOException {
        this.writer = new FileWriter(path);

        writeMatrix(matrix.getMatrix());

        this.writer.flush();
        this.writer.close();

        System.out.println("Matica uspesne zapisana na " + path);
    }

    public void exportToCSV(String nameFile, Matrix<? extends Number> matrix)  {
        matrixChecker.throwExceptionIfNull(nameFile);
        matrixChecker.throwExceptionIfNull(matrix);

        final String csvFilePath = this.path + nameFile + ".csv";

        try{
            writeToCSV(csvFilePath, matrix);
        } catch (IOException e){
            System.out.println("Matica nezapisana na " + csvFilePath);
        }
    }



}
