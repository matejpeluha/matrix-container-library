package matrixLib.matrixFunctions;

import matrixLib.Matrix;
import matrixLib.matrixFunctions.calculators.MatrixAdditioner;
import matrixLib.matrixFunctions.calculators.MatrixDeterminant;
import matrixLib.matrixFunctions.calculators.MatrixMultiplier;
import matrixLib.matrixFunctions.calculators.MatrixSubtractioner;
import matrixLib.support.MatrixChecker;
import matrixLib.support.operations.GenericOperations;
import matrixLib.support.operations.OperationsChooser;

@SuppressWarnings("unchecked")
public class MatrixCalculator<MatrixType extends Number> {
    MatrixChecker matrixChecker = new MatrixChecker();

    private GenericOperations<MatrixType> chooseOperation(MatrixType[][] matrix1){
        this.matrixChecker.findPossibleExceptionsInMatrix(matrix1);
        this.matrixChecker.throwExceptionIfNull(matrix1[0]);
        this.matrixChecker.throwExceptionIfNull(matrix1[0][0]);

        OperationsChooser operationsChooser = new OperationsChooser();
        Class<? extends Number> matrixType = matrix1[0][0].getClass();

        return (GenericOperations<MatrixType>) operationsChooser.choose(matrixType);
    }

    public MatrixType getDeterminant(MatrixType[][] matrix){
        matrixChecker.findPossibleExceptionsInMatrix(matrix);

        Matrix<MatrixType> objectMatrix = new Matrix<MatrixType>(matrix);

        return getDeterminant(objectMatrix);
    }

    public MatrixType getDeterminant(Matrix<MatrixType> matrix){
        GenericOperations<MatrixType> operations = chooseOperation(matrix.getMatrix());

        MatrixDeterminant<MatrixType> determinant = new MatrixDeterminant<>(operations);

        return determinant.getDeterminant(matrix);
    }

    public MatrixType[][] subtraction(MatrixType[][] matrix1, MatrixType[][] matrix2){
        GenericOperations<MatrixType> operations = chooseOperation(matrix1);

        MatrixSubtractioner<MatrixType> subtractioner = new MatrixSubtractioner<>(operations);

        return subtractioner.subtraction(matrix1, matrix2);
    }

    public Matrix<MatrixType> subtraction(Matrix<MatrixType> matrix1, Matrix<MatrixType> matrix2){
        MatrixType[][] result = subtraction(matrix1.getMatrix(), matrix2.getMatrix());

        return new Matrix<>(result);
    }


    public MatrixType[][] addition(MatrixType[][] matrix1, MatrixType[][] matrix2){
        GenericOperations<MatrixType> operations = chooseOperation(matrix1);

        MatrixAdditioner<MatrixType> additioner = new MatrixAdditioner<>(operations);

        return additioner.addition(matrix1, matrix2);
    }

    public Matrix<MatrixType> addition(Matrix<MatrixType> matrix1, Matrix<MatrixType> matrix2){
        MatrixType[][] result = addition(matrix1.getMatrix(), matrix2.getMatrix());

        return new Matrix<>(result);
    }

    public MatrixType[][] multiplication(MatrixType[][] matrix1, MatrixType[][] matrix2){
        GenericOperations<MatrixType> operations = chooseOperation(matrix1);

        MatrixMultiplier<MatrixType> multiplier = new MatrixMultiplier<>(operations);

        return multiplier.multiplication(matrix1, matrix2);
    }

    public Matrix<MatrixType> multiplication(Matrix<MatrixType> matrix1, Matrix<MatrixType> matrix2){
        MatrixType[][] result = multiplication(matrix1.getMatrix(), matrix2.getMatrix());

        return new Matrix<>(result);
    }
}
