package matrixLib.matrixFunctions;

import matrixLib.Matrix;
import matrixLib.support.MatrixChecker;
import matrixLib.support.operations.GenericOperations;

import java.awt.*;
import java.util.*;


public class MatrixGenerator<MatrixType extends Number>{
    private final GenericOperations<MatrixType> operations;
    protected Dimension size;

    private final MatrixChecker matrixChecker;


    public MatrixGenerator(int numColumns, int numRows, GenericOperations<MatrixType> operations){
        this(new Dimension(numColumns, numRows), operations);
    }

    public MatrixGenerator(Dimension size, GenericOperations<MatrixType> operations) {
        this.matrixChecker = new MatrixChecker();

        this.matrixChecker.throwExceptionIfNull(size);
        this.size = size;

        this.matrixChecker.throwExceptionIfNull(operations);
        this.operations = operations;
    }


    public MatrixType[][] createEmptyArrayMatrix(){
        int numRows= this.size.height;
        int numColumns = this.size.width;

        return this.operations.createArrayMatrix(numRows,numColumns);
    }


    private MatrixType[] generateOneRow(MatrixType[] matrixRow){
        for(int idColumn = 0; idColumn < this.size.width; idColumn++) {
            matrixRow[idColumn] = this.operations.randomNumber();
        }

        return matrixRow;
    }

    public MatrixType[][] randomArrayMatrix(){
        MatrixType[][] matrix = createEmptyArrayMatrix();

        for(int idRow = 0; idRow < this.size.height; idRow++)
            matrix[idRow] = generateOneRow(matrix[idRow]);

        return matrix;
    }

    public Matrix<MatrixType> randomMatrix(){
        MatrixType[][] matrix = randomArrayMatrix();

        return new Matrix<MatrixType>(matrix);
    }

    public void setSize(Dimension size){
        this.matrixChecker.throwExceptionIfNull(size);
        this.size = size;
    }

    public void setSize(int numberColumns, int numberRows){
        Dimension size = new Dimension(numberColumns, numberRows);
        setSize(size);
    }

}


