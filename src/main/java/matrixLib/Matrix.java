package matrixLib;


import lombok.Getter;
import matrixLib.matrixFunctions.*;
import matrixLib.support.MatrixChecker;
import matrixLib.support.operations.GenericOperations;
import matrixLib.support.operations.OperationsChooser;
import java.awt.*;


@SuppressWarnings("unchecked")
@Getter
public class Matrix<MatrixType extends Number>{
    private MatrixType[][] matrix;

    private final GenericOperations<MatrixType> operations;
    private final MatrixChecker matrixChecker = new MatrixChecker();
    private final MatrixCalculator<MatrixType> calculator = new MatrixCalculator<>();

    public Matrix(GenericOperations<MatrixType> operations){
        matrixChecker.throwExceptionIfNull(operations);
        this.operations = operations;
    }

    public Matrix(MatrixType[][] matrix){
        setMatrix(matrix);

        final Class<? extends Number> matrixType = matrix[0][0].getClass();

        this.operations = chooseOperation(matrixType);
    }

    private GenericOperations<MatrixType> chooseOperation(Class<? extends Number> matrixType){
        matrixChecker.throwExceptionIfNull(matrixType);

        OperationsChooser operationsChooser = new OperationsChooser();

        return (GenericOperations<MatrixType>) operationsChooser.choose(matrixType);
    }

    public MatrixType getDeterminant(){
        return calculator.getDeterminant(this);
    }

    public void importFromCSV(String nameFile){
        MatrixImporter<MatrixType> importer = new MatrixImporter<>(this.operations);
        Matrix<MatrixType> importedMatrix = importer.importFromCSV(nameFile);

        if(importedMatrix.getMatrix() != null)
            this.matrix = importedMatrix.getMatrix();
    }

    public void exportToCSV(String nameFile){
        matrixChecker.throwExceptionIfNull(nameFile);

        MatrixExporter exporter = new MatrixExporter();
        exporter.exportToCSV(nameFile, this);
    }

    public void exportToCSV(String path, String nameFile){
        matrixChecker.throwExceptionIfNull(path);
        matrixChecker.throwExceptionIfNull(nameFile);

        MatrixExporter exporter = new MatrixExporter(path);
        exporter.exportToCSV(nameFile, this);
    }

    public Matrix<MatrixType> minus(Matrix<MatrixType> matrix2){
        return this.calculator.subtraction(this, matrix2);
    }

    public Matrix<MatrixType> plus(Matrix<MatrixType> matrix2){
        return this.calculator.addition(this, matrix2);
    }

    public Matrix<MatrixType> multiply(Matrix<MatrixType> matrix2){
        return this.calculator.multiplication(this, matrix2);
    }


    public void generateRandom(int numColumns, int numRows){
        Dimension size = new Dimension(numColumns, numRows);

        MatrixGenerator<MatrixType> generator = new MatrixGenerator<>(size, this.operations);

        this.matrix = generator.randomArrayMatrix();
    }

    public void printMatrix(){
        MatrixPrinter printer = new MatrixPrinter();
        printer.printMatrix(this.matrix);
    }

    public void setMatrix(MatrixType[][] matrix){
        this.matrixChecker.findPossibleExceptionsInMatrix(matrix);
        this.matrixChecker.throwExceptionIfBadFormatOfMatrix(matrix);

        this.matrix = matrix;
    }

    public void setMatrixElement(MatrixType number, int row, int column) throws IndexOutOfBoundsException{
        matrixChecker.throwExceptionIfNull(number);

        if(row < 0 || row >= this.matrix.length)
            throw new IndexOutOfBoundsException("Matica nema dany pocet riadkov.");
        else if(column < 0 || column >= this.matrix[row].length)
            throw new IndexOutOfBoundsException("Matica nema dany pocet stlpcov");

        this.matrix[row][column] = number;
    }


}
