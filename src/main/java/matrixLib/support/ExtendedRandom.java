package matrixLib.support;

import matrixLib.support.datatypes.HalfByte;
import matrixLib.support.datatypes.UnsignedInteger;

import java.util.Random;

public class ExtendedRandom extends Random {

    public ExtendedRandom(){
        super();
    }

    @Override
    public float nextFloat(){
        float number = super.nextFloat();
        number *= nextInt();

        return number;
    }

    @Override
    public double nextDouble(){
        double number = super.nextDouble();
        number *= nextInt();

        return number;
    }


    public UnsignedInteger nextUnsignedInt(){
        int intNumber = nextInt(Integer.MAX_VALUE);

        return new UnsignedInteger(intNumber);
    }

    public Short nextShort(){
        return (short) nextInt(Short.MAX_VALUE + 1);
    }

    public Byte nextByte(){
        byte[] bytes = new byte[1];
        this.nextBytes(bytes);

        return (Byte) bytes[0];
    }

    public HalfByte nextHalfByte(){
        final int MAX = 17;
        final int MIN = -17;

        int intNumber = nextInt(MAX - MIN) + MIN;

        return new HalfByte(intNumber);
    }
}
