package matrixLib.support.operations;

import matrixLib.support.ExtendedRandom;
import matrixLib.support.datatypes.UnsignedInteger;

public class UnsignedIntegerOperations implements GenericOperations<UnsignedInteger> {
    @Override
    public UnsignedInteger plus(UnsignedInteger number1, UnsignedInteger number2) {
        return new UnsignedInteger(number1.intValue() + number2.intValue());
    }

    @Override
    public UnsignedInteger minus(UnsignedInteger number1, UnsignedInteger number2) {
        return new UnsignedInteger(number1.intValue() - number2.intValue());
    }

    @Override
    public UnsignedInteger multiply(UnsignedInteger number1, UnsignedInteger number2) {
        return new UnsignedInteger(number1.intValue() * number2.intValue());
    }

    @Override
    public UnsignedInteger initializeZero() {
        return new UnsignedInteger(0);
    }

    @Override
    public UnsignedInteger[] createArray(int size) {
        return new UnsignedInteger[size];
    }

    @Override
    public UnsignedInteger[][] createArrayMatrix(int numRows, int numColumns) {
        return new UnsignedInteger[numRows][numColumns];
    }

    @Override
    public UnsignedInteger randomNumber() {
        ExtendedRandom random = new ExtendedRandom();
        return random.nextUnsignedInt();
    }

    @Override
    public UnsignedInteger castFromString(String number) throws NumberFormatException {
        try {
            return UnsignedInteger.parseUnsInt(number.trim());
        }
        catch (NumberFormatException nfe)
        {
            throw new NumberFormatException();
        }
    }

    @Override
    public UnsignedInteger increment(UnsignedInteger number) {
        return new UnsignedInteger(number.intValue() + 1);
    }

    @Override
    public UnsignedInteger decrement(UnsignedInteger number) {
        return new UnsignedInteger(number.intValue() - 1);
    }

    @Override
    public UnsignedInteger pow(UnsignedInteger number1, UnsignedInteger number2) {
        return new UnsignedInteger(number1.intValue() ^ number2.intValue());
    }
}
