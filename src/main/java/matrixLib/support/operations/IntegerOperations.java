package matrixLib.support.operations;

import matrixLib.support.ExtendedRandom;

public class IntegerOperations implements  GenericOperations<Integer>{

    @Override
    public Integer plus(Integer number1, Integer number2) {
        return number1 + number2;
    }

    @Override
    public Integer minus(Integer number1, Integer number2) {
        return number1 - number2;
    }

    @Override
    public Integer multiply(Integer number1, Integer number2) {
        return number1 * number2 ;
    }

    @Override
    public Integer initializeZero() {
        return 0;
    }

    @Override
    public Integer[] createArray(int size) {
        return new Integer[size];
    }

    @Override
    public Integer[][] createArrayMatrix(int numRows, int numColumns) {
        return new Integer[numRows][numColumns];
    }

    @Override
    public Integer randomNumber() {
        ExtendedRandom random = new ExtendedRandom();
        return random.nextInt();
    }

    @Override
    public Integer castFromString(String number) throws NumberFormatException {
        try
        {
            return Integer.parseInt(number.trim());
        }
        catch (NumberFormatException nfe)
        {
            throw new NumberFormatException();
        }
    }

    @Override
    public Integer increment(Integer number) {
        return ++number;
    }

    @Override
    public Integer decrement(Integer number) {
        return --number;
    }

    @Override
    public Integer pow(Integer number1, Integer number2) {
        return number1 ^ number2;
    }
}
