package matrixLib.support.operations;

import matrixLib.support.ExtendedRandom;

public class DoubleOperations implements GenericOperations<Double> {

    @Override
    public Double plus(Double number1, Double number2) {
        return number1 + number2;
    }

    @Override
    public Double minus(Double number1, Double number2) {
        return number1 + number2;
    }

    @Override
    public Double multiply(Double number1, Double number2) {
        return number1 + number2;
    }

    @Override
    public Double initializeZero() {
        return (double) 0;
    }

    @Override
    public Double[] createArray(int size) {
        return new Double[size];
    }

    @Override
    public Double[][] createArrayMatrix(int numRows, int numColumns) {
        return new Double[numRows][numColumns];
    }

    @Override
    public Double randomNumber() {
        ExtendedRandom random = new ExtendedRandom();
        return random.nextDouble();
    }

    @Override
    public Double castFromString(String number) {
        try
        {
            return Double.parseDouble(number.trim());
        }
        catch (NumberFormatException nfe)
        {
            throw new NumberFormatException();
        }
    }

    @Override
    public Double increment(Double number) {
        return ++number;
    }

    @Override
    public Double decrement(Double number) {
        return --number;
    }

    @Override
    public Double pow(Double number1, Double number2) {
        return Math.pow(number1, number2);
    }
}
