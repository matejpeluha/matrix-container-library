package matrixLib.support.operations;

import matrixLib.support.ExtendedRandom;
import matrixLib.support.datatypes.HalfByte;

public class HalfByteOperations implements GenericOperations<HalfByte> {

    @Override
    public HalfByte plus(HalfByte number1, HalfByte number2) {
        return new HalfByte(number1.byteValue() + number2.byteValue());
    }

    @Override
    public HalfByte minus(HalfByte number1, HalfByte number2) {
        return new HalfByte(number1.byteValue() - number2.byteValue());
    }

    @Override
    public HalfByte multiply(HalfByte number1, HalfByte number2) {
        return new HalfByte(number1.byteValue() * number2.byteValue());
    }

    @Override
    public HalfByte initializeZero() {
        return new HalfByte(0);
    }

    @Override
    public HalfByte[] createArray(int size) {
        return new HalfByte[size];
    }

    @Override
    public HalfByte[][] createArrayMatrix(int numRows, int numColumns) {
        return new HalfByte[numRows][numColumns];
    }

    @Override
    public HalfByte randomNumber() {
        ExtendedRandom random = new ExtendedRandom();
        return random.nextHalfByte();
    }

    @Override
    public HalfByte castFromString(String number) throws NumberFormatException {
        try
        {
            return HalfByte.parseHalfByte(number.trim());
        }
        catch (NumberFormatException nfe)
        {
            throw new NumberFormatException();
        }
    }

    @Override
    public HalfByte increment(HalfByte number) {
        return new HalfByte(number.intValue() + 1);
    }

    @Override
    public HalfByte decrement(HalfByte number) {
        return new HalfByte(number.intValue() - 1);
    }

    @Override
    public HalfByte pow(HalfByte number1, HalfByte number2) {
        return new HalfByte((number1.intValue() ^ number2.intValue()));
    }
}
