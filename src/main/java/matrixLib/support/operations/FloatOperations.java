package matrixLib.support.operations;

import matrixLib.support.ExtendedRandom;

public class FloatOperations implements GenericOperations<Float> {


    @Override
    public Float plus(Float number1, Float number2) {
        return number1 + number2;
    }

    @Override
    public Float minus(Float number1, Float number2) {
        return number1 + number2;
    }

    @Override
    public Float multiply(Float number1, Float number2) {
        return number1 * number2;
    }

    @Override
    public Float initializeZero() {
        return (float) 0;
    }

    @Override
    public Float[] createArray(int size) {
        return new Float[size];
    }

    @Override
    public Float[][] createArrayMatrix(int numRows, int numColumns) {
        return new Float[numRows][numColumns];
    }

    @Override
    public Float randomNumber() {
        ExtendedRandom random = new ExtendedRandom();
        return random.nextFloat();
    }

    @Override
    public Float castFromString(String number) throws NumberFormatException {
        try
        {
            return Float.parseFloat(number.trim());
        }
        catch (NumberFormatException nfe)
        {
            throw new NumberFormatException();
        }
    }

    @Override
    public Float increment(Float number) {
        return ++number;
    }

    @Override
    public Float decrement(Float number) {
        return --number;
    }

    @Override
    public Float pow(Float number1, Float number2) {
        return (float) Math.pow(number1, number2);
    }
}
