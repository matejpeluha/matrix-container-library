package matrixLib.support.operations;

import matrixLib.support.ExtendedRandom;

public class LongOperations implements GenericOperations<Long> {
    @Override
    public Long plus(Long number1, Long number2) {
        return number1 + number2 ;
    }

    @Override
    public Long minus(Long number1, Long number2) {
        return number1 - number2;
    }

    @Override
    public Long multiply(Long number1, Long number2) {
        return number1 * number2;
    }

    @Override
    public Long initializeZero() {
        return (long) 0;
    }

    @Override
    public Long[] createArray(int size) {
        return new Long[size];
    }

    @Override
    public Long[][] createArrayMatrix(int numRows, int numColumns) {
        return new Long[numRows][numColumns];
    }

    @Override
    public Long randomNumber() {
        ExtendedRandom random = new ExtendedRandom();
        return random.nextLong();
    }

    @Override
    public Long castFromString(String number) throws NumberFormatException {
        try {
            return Long.parseLong(number.trim());
        }
        catch (NumberFormatException nfe)
        {
            throw new NumberFormatException();
        }
    }

    @Override
    public Long increment(Long number) {
        return ++number;
    }

    @Override
    public Long decrement(Long number) {
        return --number;
    }

    @Override
    public Long pow(Long number1, Long number2) {
        return number1 ^ number2;
    }
}
