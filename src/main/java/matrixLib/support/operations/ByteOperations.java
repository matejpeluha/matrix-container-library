package matrixLib.support.operations;

import matrixLib.support.ExtendedRandom;

public class ByteOperations implements GenericOperations<Byte> {

    @Override
    public Byte plus(Byte number1, Byte number2) {
        return (byte) (number1 + number2);
    }

    @Override
    public Byte minus(Byte number1, Byte number2) {
        return (byte) (number1 - number2);
    }

    @Override
    public Byte multiply(Byte number1, Byte number2) {
        return (byte) (number1 * number2);
    }

    @Override
    public Byte initializeZero() {
        return (byte) 0;
    }

    @Override
    public Byte[] createArray(int size) {
        return new Byte[size];
    }

    @Override
    public Byte[][] createArrayMatrix(int numRows, int numColumns) {
        return new Byte[numRows][numColumns];
    }

    @Override
    public Byte randomNumber() {
        ExtendedRandom random = new ExtendedRandom();
        return random.nextByte();
    }

    @Override
    public Byte castFromString(String number) throws NumberFormatException{
        try
        {
            return Byte.parseByte(number.trim());
        }
        catch (NumberFormatException nfe)
        {
            throw new NumberFormatException();
        }
    }

    @Override
    public Byte increment(Byte number) {
        return ++number;
    }

    @Override
    public Byte decrement(Byte number) {
        return --number;
    }

    @Override
    public Byte pow(Byte number1, Byte number2) {
        return (byte) (number1 ^ number2);
    }

}
