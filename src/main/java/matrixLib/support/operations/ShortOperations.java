package matrixLib.support.operations;

import matrixLib.support.ExtendedRandom;

public class ShortOperations implements GenericOperations<Short> {
    @Override
    public Short plus(Short number1, Short number2) {
        return (short) (number1 + number2);
    }

    @Override
    public Short minus(Short number1, Short number2) {
        return (short) (number1 - number2);
    }

    @Override
    public Short multiply(Short number1, Short number2) {
        return (short) (number1 * number2);
    }

    @Override
    public Short initializeZero() {
        return (short) 0;
    }

    @Override
    public Short[] createArray(int size) {
        return new Short[size];
    }

    @Override
    public Short[][] createArrayMatrix(int numRows, int numColumns) {
        return new Short[numRows][numColumns];
    }

    @Override
    public Short randomNumber() {
        ExtendedRandom random = new ExtendedRandom();
        return random.nextShort();
    }

    @Override
    public Short castFromString(String number) throws NumberFormatException {
        try {
            return Short.parseShort(number.trim());
        }
        catch (NumberFormatException nfe)
        {
            throw new NumberFormatException();
        }
    }

    @Override
    public Short increment(Short number) {
        return ++number;
    }

    @Override
    public Short decrement(Short number) {
        return --number;
    }

    @Override
    public Short pow(Short number1, Short number2) {
        return (short) (number1 ^ number2);
    }
}
