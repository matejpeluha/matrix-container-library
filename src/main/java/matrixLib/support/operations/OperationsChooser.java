package matrixLib.support.operations;

import matrixLib.support.datatypes.HalfByte;
import matrixLib.support.datatypes.UnsignedInteger;

public class OperationsChooser {


    public GenericOperations<? extends Number> choose(Class<? extends Number> type){
        if(type == HalfByte.class)
            return new HalfByteOperations();
        else if(type == Byte.class)
            return new ByteOperations();
        else if(type == Short.class)
            return new ShortOperations();
        else if(type == Integer.class)
            return new IntegerOperations();
        else if(type == UnsignedInteger.class)
            return new UnsignedIntegerOperations();
        else if(type == Long.class)
            return new LongOperations();
        else if(type == Float.class)
            return new FloatOperations();
        else if(type == Double.class)
            return new DoubleOperations();
        else if(type == null)
            throw new NullPointerException();
        else
            throw new IllegalArgumentException();
    }
}
