package matrixLib.support.operations;

public interface GenericOperations<MatrixType extends Number> {
    public MatrixType plus(MatrixType number1, MatrixType number2);
    public MatrixType minus(MatrixType number1, MatrixType number2);
    public MatrixType multiply(MatrixType number1, MatrixType number2);
    public MatrixType initializeZero();
    public MatrixType[] createArray(int size);
    public MatrixType[][] createArrayMatrix(int numRows, int numColumns);
    public MatrixType randomNumber();
    public MatrixType castFromString(String number) throws NumberFormatException;
    public MatrixType increment(MatrixType number);
    public MatrixType decrement(MatrixType number);
    public MatrixType pow(MatrixType number1, MatrixType number2);
}
