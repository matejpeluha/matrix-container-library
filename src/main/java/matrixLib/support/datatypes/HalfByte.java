package matrixLib.support.datatypes;

public class HalfByte extends Number{
    private byte number;
    private final int MAX_SIZE = 16;
    private final int MIN_SIZE = -16;



    public HalfByte(int number) {
        setNumber(number);
    }

    public static HalfByte parseHalfByte(String trim) {
        return new HalfByte(Byte.parseByte(trim));
    }

    private int lowerOverFlow(int number){
        int afterOverFlow = MAX_SIZE + 1 - (-number - MAX_SIZE);

        if(afterOverFlow < MIN_SIZE)
            return lowerOverFlow(afterOverFlow);

        return afterOverFlow;
    }


    private int upperOverFlow(int number){
        int afterOverFlow = MIN_SIZE - 1 + (number - MAX_SIZE);

        if(afterOverFlow > MAX_SIZE)
            return upperOverFlow(afterOverFlow);

        return afterOverFlow;
    }

    public void setNumber(int number){
        if(number < MIN_SIZE  )
            number = lowerOverFlow(number);
        else if(number > MAX_SIZE)
            number = upperOverFlow(number);

        this.number = (byte) number;
    }

    @Override
    public String toString() {
        return "" + number;
    }

    @Override
    public byte byteValue() {
        return this.number;
    }

    @Override
    public short shortValue() {
        return this.number;
    }

    @Override
    public int intValue() {
        return this.number;
    }

    @Override
    public long longValue() {
        return this.number;
    }

    @Override
    public float floatValue() {
        return this.number;
    }

    @Override
    public double doubleValue() {
        return this.number;
    }
}
