package matrixLib.support.datatypes;

public class UnsignedInteger extends Number{
    private int number;

    public UnsignedInteger(int number){
        setNumber(number);
    }

    public static UnsignedInteger parseUnsInt(String trim) {
        return new UnsignedInteger(Integer.parseInt(trim));
    }

    private int overFlow(int number){
        return Integer.MAX_VALUE - 1 + number;
    }

    public void setNumber(int number){
        if(number < 0)
            number = overFlow(number);

        this.number = number;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "" + number;
    }

    @Override
    public byte byteValue() {
        return super.byteValue();
    }

    @Override
    public short shortValue() {
        return super.shortValue();
    }

    @Override
    public int intValue() {
        return this.number;
    }

    @Override
    public long longValue() {
        return this.number;
    }

    @Override
    public float floatValue() {
        return this.number;
    }

    @Override
    public double doubleValue() {
        return this.number;
    }
}
