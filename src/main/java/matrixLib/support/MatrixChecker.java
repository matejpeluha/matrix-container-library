package matrixLib.support;

import java.io.File;
import java.io.IOException;

public class MatrixChecker {

    public void throwExceptionIfNotMatrixType(Object object) throws IllegalArgumentException{
        if(object instanceof Number[][] )
            return;

        throw new IllegalArgumentException("Neplatny datovy typ pre maticu. Platne datove typy: " +
                "Byte, Short, Integer, SignedInteger, Long, Float, Double");
    }

    public void throwExceptionIfNull(Object object) throws NullPointerException{
        if(object == null)
            throw new NullPointerException();
    }


    private void throwExceptionIfDifferentRowSize(Number[] row, int rowSize) throws IllegalArgumentException{
        throwExceptionIfNull(row);

        if(row.length != rowSize)
            throw new IllegalArgumentException();
    }

    public void throwExceptionIfBadFormatOfMatrix(Number[][] matrix){
        throwExceptionIfNull(matrix);

        int rowSize = matrix[0].length;

        for(Number[] row : matrix)
            throwExceptionIfDifferentRowSize(row, rowSize);
    }

    public void findPossibleExceptionsInMatrix(Number[][] object){
        throwExceptionIfNull(object);
        throwExceptionIfNotMatrixType(object);
        throwExceptionIfBadFormatOfMatrix(object);
    }

    public void throwExceptionIfMatricesCanNotBeMultiplied(Number[][] matrix1, Number[][] matrix2) throws IllegalArgumentException{
        findPossibleExceptionsInMatrix(matrix1);
        findPossibleExceptionsInMatrix(matrix2);

        throwExceptionIfNull(matrix1[0]);

        int numberOfRows1 = matrix1[0].length;
        int numberOfColumns2 = matrix2.length;

        if(numberOfRows1 != numberOfColumns2)
            throw new IllegalArgumentException("Matice nie je mozne nasobit");

    }

    public void throwExceptionIfAdditionIsNotPossible(Number[][] matrix1, Number[][] matrix2) throws IllegalArgumentException{
        findPossibleExceptionsInMatrix(matrix1);
        findPossibleExceptionsInMatrix(matrix2);

        if(matrix1.length != matrix2.length)
            throw new IllegalArgumentException("Matice nejde scitat.");

        else if(matrix1[0].length != matrix2[0].length)
            throw new IllegalArgumentException("Matice nejde scitat.");
    }

    public void throwExceptionIfDeterminantIsNotPossible(Number[][] matrix)throws IllegalArgumentException{
        findPossibleExceptionsInMatrix(matrix);
        throwExceptionIfNull(matrix[0]);

        if(matrix.length != matrix[0].length)
            throw new IllegalArgumentException("Zly rozmer matice pre vypocet determinantu");
    }

}
